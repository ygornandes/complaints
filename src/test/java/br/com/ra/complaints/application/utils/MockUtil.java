package br.com.ra.complaints.application.utils;

import br.com.ra.complaints.domain.model.Complaint;
import br.com.ra.complaints.infrastructure.mongodb.documents.ComplaintDocument;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MockUtil {
    public static ComplaintDocument createComplain(String id) {
        return ComplaintDocument.builder()
                .id(id)
                .title("Teste "+id)
                .description("Descrição de teste")
                .locale("São Paulo")
                .company("xpto")
                .createdDate(LocalDateTime.now())
                .build();
    }

    public static Optional<Complaint> getOptionalComplaint(String id) {
        return Optional.of(getComplaint(id));
    }

    public static Complaint getComplaint(String id) {
        return Complaint
                .builder()
                .id(id)
                .title("Teste "+id)
                .description("Descrição de teste")
                .locale("São Paulo")
                .company("xpto")
                .createdDate(LocalDateTime.now())
                .build();
    }

    public static Optional<List<Complaint>> getListOptinalComplaint() {
        return Optional.of(IntStream.range(0,6).mapToObj(i -> getComplaint(Integer.toString(i))).collect(Collectors.toList()));
    }
}
