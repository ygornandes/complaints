package br.com.ra.complaints.application.web.controller;

import br.com.ra.complaints.application.utils.MockUtil;
import br.com.ra.complaints.infrastructure.mongodb.repository.MongoComplaintRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static br.com.ra.complaints.application.utils.Constants.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc()
public class ComplaintControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MongoComplaintRepository repository;

    @Before
    public void setUp(){
        inputComplaintS();
    }

    private void inputComplaintS() {
        repository.save(MockUtil.createComplain(COMPLAIN_ONE));
        repository.save(MockUtil.createComplain(COMPLAIN_TWO));
        repository.save(MockUtil.createComplain(COMPLAIN_THREE));
        repository.save(MockUtil.createComplain(COMPLAIN_FOUR));
        repository.save(MockUtil.createComplain(COMPLAIN_FIVE));
    }

    @Test
    public void shouldCreateComplainWithSuccess() throws Exception {
        mockMvc.perform(post("/api/complaints")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(getRequestBody("json/createComplaint.json")))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void shouldReturnBadRequestBecauseInvalidPayload() throws Exception {
        mockMvc.perform(post("/api/complaints")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(getRequestBody("json/invalidPayload.json")))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.errors").isArray())
                .andExpect(jsonPath("$.errors", hasSize(4)))
                .andExpect(jsonPath("$.errors", hasItem("Title can not be blank.")))
                .andExpect(jsonPath("$.errors", hasItem("Description can not be blank.")))
                .andExpect(jsonPath("$.errors", hasItem("Locale can not be blank.")))
                .andExpect(jsonPath("$.errors", hasItem("Company can not be blank.")));
    }

    @Test
    public void shouldReturnBadRequestBecauseWithoutBody() throws Exception {
        mockMvc.perform(post("/api/complaints")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message",is("Request body is required.")));
    }

    @Test
    public void shouldUpdateComplaintWithSuccess() throws Exception {
        mockMvc.perform(put("/api/complaints/{id}", COMPLAIN_ONE)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(getRequestBody("json/updateComplaint.json")))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldReturnNotFoundWhenTryingToUpdateComplaintFound() throws Exception {
        mockMvc.perform(put("/api/complaints/{id}",COMPLAIN_NOTFOUND)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(getRequestBody("json/updateComplaint.json")))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",is("Complaint not found.")));
    }

    @Test
    public void shouldResturnComplaintByIdWithSuccess() throws Exception {
        mockMvc.perform(get("/api/complaints/{id}", COMPLAIN_TWO)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(jsonPath("$.id").value(is(COMPLAIN_TWO) ))//is(COMPLAIN_TWO)))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnNotFoundWhenComplaintFound() throws Exception {
        mockMvc.perform(get("/api/complaints/{id}",COMPLAIN_NOTFOUND)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",is("Complaint not found.")));
    }

    @Test
    public void shouldReturnComplainsByPaginationWithSucces() throws Exception {
        mockMvc.perform(get("/api/complaints").param("page","0").param("size","10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$",hasSize(6)))
                .andExpect(jsonPath("$..id",hasItem(COMPLAIN_FOUR)));
    }

    @Test
    public void shouldReturnNotFoundWhenPageFound() throws Exception {
        mockMvc.perform(get("/api/complaints").param("page","1").param("size","10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",is("Page not found.")));
    }

    @Test
    public void shouldDeleteByIdWithSuccess() throws Exception {
        mockMvc.perform(delete("/api/complaints/{id}", COMPLAIN_FOUR)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldReturnComplainsByCompanyAndLocaleWithSucces() throws Exception {
        mockMvc.perform(get("/api/complaints")
                .param("company","xpto")
                .param("locale","São Paulo")
                .param("page","0")
                .param("size","10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$",hasSize(6)))
                .andExpect(jsonPath("$..id",hasItem(COMPLAIN_FOUR)));
    }

    @Test
    public void shouldReturnNotFoundBecauseCompanyAndLocaleNotFound() throws Exception {
        mockMvc.perform(get("/api/complaints")
                .param("company","xpto")
                .param("locale","São Paulo")
                .param("page","1")
                .param("size","10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",is("Page not found.")));
    }

    @Test
    public void shouldReturnComplainsByCompanyWithSucces() throws Exception {
        mockMvc.perform(get("/api/complaints")
                .param("company","xpto")
                .param("page","0")
                .param("size","10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$",hasSize(6)))
                .andExpect(jsonPath("$..id",hasItem(COMPLAIN_FOUR)));
    }

    @Test
    public void shouldReturnNotFoundBecauseCompanyNotFound() throws Exception {
        mockMvc.perform(get("/api/complaints")
                .param("company","xpto")
                .param("page","1")
                .param("size","10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message",is("Page not found.")));
    }

    @Test
    public void shouldReturnComplainsByLocaleWithSucces() throws Exception {
        mockMvc.perform(get("/api/complaints")
                .param("locale","São Paulo")
                .param("page","0")
                .param("size","10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$",hasSize(6)))
                .andExpect(jsonPath("$..id",hasItem(COMPLAIN_FOUR)));
    }

    @Test
    public void shouldReturnNotFoundBecauseLocaleNotFound() throws Exception {
        mockMvc.perform(get("/api/complaints")
                .param("locale", "Campinas")
                .param("page", "0")
                .param("size", "10")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message", is("Page not found.")));
    }

    @Test
    public void shouldReturnComplainCountByCompanyAndLocaleWithTotal() throws Exception {
        mockMvc.perform(get("/api/complaints/count")
                .param("company","xpto")
                .param("locale","São Paulo")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total",is(6)));
    }

    @Test
    public void shouldReturnComplainCountByCompanyAndLocaleWithTotalZero() throws Exception {
        mockMvc.perform(get("/api/complaints/count")
                .param("company","xpto")
                .param("locale","Rio de Janeiro")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total",is(0)));
    }

    @Test
    public void shouldReturnComplainCountByLocaleWithTotal() throws Exception {
        mockMvc.perform(get("/api/complaints/count")
                .param("locale","São Paulo")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total",is(6)));
    }

    @Test
    public void shouldReturnComplainCountByLocaleWithTotalZero() throws Exception {
        mockMvc.perform(get("/api/complaints/count")
                .param("locale","Rio de Janeiro")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.total",is(0)));
    }

    private String getRequestBody(String path) throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(path);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
            return br.lines().collect(Collectors.joining(System.lineSeparator()));
        }
    }

}