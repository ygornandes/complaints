package br.com.ra.complaints.domain.service.impl;

import br.com.ra.complaints.application.config.properties.MessagesProperty;
import br.com.ra.complaints.application.exception.ComplaintNotFoundException;
import br.com.ra.complaints.application.utils.MockUtil;
import br.com.ra.complaints.domain.model.Complaint;
import br.com.ra.complaints.domain.repository.ComplaintRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static br.com.ra.complaints.application.utils.Constants.COMPLAIN_ONE;
import static br.com.ra.complaints.application.utils.MockUtil.getComplaint;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ComplaintServiceImplTest {

    private ComplaintServiceImpl service;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    private ComplaintRepository repository;

    @Before
    public void setUp(){
        service = new ComplaintServiceImpl(repository,new MessagesProperty());
    }

    @Test
    public void shouldReturnComplaintByIdWithSuccess() {
        when(repository.getComplaint(anyString())).thenReturn(MockUtil.getOptionalComplaint(COMPLAIN_ONE));
        Complaint complaint = service.getComplaint(COMPLAIN_ONE);
        assertThat(complaint,is(notNullValue()));
        assertThat(complaint.getId(),is(COMPLAIN_ONE));

        verify(repository,atLeastOnce()).getComplaint(anyString());
    }

    @Test
    public void shouldReturnComplaintNotFoundException() {
        when(repository.getComplaint(anyString())).thenReturn(Optional.empty());
        thrown.expect(ComplaintNotFoundException.class);

        service.getComplaint(COMPLAIN_ONE);

        verify(repository,atLeastOnce()).getComplaint(anyString());
    }

    @Test
    public void shouldDeleteComplainWithSuccess() {
        service.delete(COMPLAIN_ONE);
        verify(repository,atLeastOnce()).delete(anyString());
    }

    @Test
    public void shouldReturnComplaintsByPaginationWithSuccess() {
        when(repository.getComplaints(anyInt(),anyInt())).thenReturn(MockUtil.getListOptinalComplaint());

        List<Complaint> complaints = service.getComplaints(0, 10);

        assertThat(complaints.size(),is(6));
        assertThat(complaints.isEmpty(),is(false));

        verify(repository,atLeastOnce()).getComplaints(anyInt(),anyInt());
    }

    @Test
    public void shouldReturnPageNotFound() {
        when(repository.getComplaints(anyInt(),anyInt())).thenReturn(Optional.empty());
        thrown.expect(ComplaintNotFoundException.class);
        service.getComplaints(0, 10);

        verify(repository,atLeastOnce()).getComplaints(anyInt(),anyInt());
    }

    @Test
    public void shouldCreateComplaintWithSuccess() {
        service.create(getComplaint(null));
        verify(repository,atLeastOnce()).create(any());
    }

    @Test
    public void shouldUpdateComplaintWithSuccess() {
        when(repository.getComplaint(anyString())).thenReturn(MockUtil.getOptionalComplaint(COMPLAIN_ONE));

        service.update(getComplaint(COMPLAIN_ONE),COMPLAIN_ONE);

        verify(repository,atLeastOnce()).getComplaint(anyString());
        verify(repository,atLeastOnce()).update(any());
    }

    @Test
    public void shouldReturnComplaintNotFoundExceptionWhenUpdate() {
        when(repository.getComplaint(anyString())).thenReturn(Optional.empty());
        thrown.expect(ComplaintNotFoundException.class);
        service.update(getComplaint(COMPLAIN_ONE),COMPLAIN_ONE);

        verify(repository,atLeastOnce()).getComplaint(anyString());
        verify(repository,never()).update(any());
    }

    @Test
    public void shouldReturnComplaintsByCompanyAndLocaleWithSuccess() {
        when(repository.getComplaints(anyString(),anyString(),anyInt(),anyInt())).thenReturn(MockUtil.getListOptinalComplaint());

        List<Complaint> complaints = service.getComplaints("xpto","São Paulo",0, 10);

        assertThat(complaints.size(),is(6));
        assertThat(complaints.isEmpty(),is(false));

        verify(repository,atLeastOnce()).getComplaints(anyString(),anyString(),anyInt(),anyInt());
    }

    @Test
    public void shouldReturnPageNotFoundWhenCompanyAndLocaleNotFound() {
        when(repository.getComplaints(anyString(),anyString(),anyInt(),anyInt())).thenReturn(Optional.empty());
        thrown.expect(ComplaintNotFoundException.class);
        service.getComplaints("xpto","São Paulo",0, 10);

        verify(repository,atLeastOnce()).getComplaints(anyString(),anyString(),anyInt(),anyInt());
    }

    @Test
    public void shouldReturnComplaintsByLocaleWithSuccess() {
        when(repository.getComplaintsByLocale(anyString(),anyInt(),anyInt())).thenReturn(MockUtil.getListOptinalComplaint());

        List<Complaint> complaints = service.getComplaintsByLocale("São Paulo",0, 10);

        assertThat(complaints.size(),is(6));
        assertThat(complaints.isEmpty(),is(false));

        verify(repository,atLeastOnce()).getComplaintsByLocale(anyString(),anyInt(),anyInt());
    }

    @Test
    public void shouldReturnPageNotFoundWhenLocaleNotFound() {
        when(repository.getComplaints(anyString(),anyString(),anyInt(),anyInt())).thenReturn(Optional.empty());
        thrown.expect(ComplaintNotFoundException.class);
        service.getComplaints("xpto","São Paulo",0, 10);

        verify(repository,atLeastOnce()).getComplaints(anyString(),anyString(),anyInt(),anyInt());
    }

    @Test
    public void shouldReturnComplaintsByCompanyWithSuccess() {
        when(repository.getComplaintsByCompany(anyString(),anyInt(),anyInt())).thenReturn(MockUtil.getListOptinalComplaint());

        List<Complaint> complaints = service.getComplaintsByCompany("xpto",0, 10);

        assertThat(complaints.size(),is(6));
        assertThat(complaints.isEmpty(),is(false));

        verify(repository,atLeastOnce()).getComplaintsByCompany(anyString(),anyInt(),anyInt());
    }

    @Test
    public void shouldReturnPageNotFoundWhenCompanyNotFound() {
        when(repository.getComplaintsByCompany(anyString(),anyInt(),anyInt())).thenReturn(Optional.empty());
        thrown.expect(ComplaintNotFoundException.class);
        service.getComplaintsByCompany("xpto",0, 10);

        verify(repository,atLeastOnce()).getComplaintsByCompany(anyString(),anyInt(),anyInt());
    }
}