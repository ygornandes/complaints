package br.com.ra.complaints.application.web.controller.requests;

import br.com.ra.complaints.domain.model.Complaint;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

import static br.com.ra.complaints.application.utis.Constants.*;

@Getter
public class ComplaintRequest {
    @NotBlank(message = TITLE_CAN_NOT_BE_BLANK)
    private String title;
    @NotBlank(message = DESCRIPTION_CAN_NOT_BE_BLANK)
    private String description;
    @NotBlank(message = LOCALE_CAN_NOT_BE_BLANK)
    private String locale;
    @NotBlank(message = COMPANY_CAN_NOT_BE_BLANK)
    private String company;

    public Complaint toComplaint() {
        return Complaint
                .builder()
                .title(this.getTitle())
                .description(this.getDescription())
                .locale(this.getLocale())
                .company(this.getCompany())
                .build();
    }
}
