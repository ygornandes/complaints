package br.com.ra.complaints.application.config.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("messages")
@AllArgsConstructor
@NoArgsConstructor
public class MessagesProperty {
    private String requestBodyRequired;
    private String complaintNotFound;
    private String pageNotFound;
    private String pathParamRequired;
    private String queryParamRequired;
}
