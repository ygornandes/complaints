package br.com.ra.complaints.application.config;


import br.com.ra.complaints.application.config.properties.MessagesProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({MessagesProperty.class})
public class ConfigurationProperty {
}
