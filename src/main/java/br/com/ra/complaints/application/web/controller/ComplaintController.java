package br.com.ra.complaints.application.web.controller;

import br.com.ra.complaints.application.web.controller.requests.ComplaintRequest;
import br.com.ra.complaints.application.web.controller.responses.ComplaintCountResponse;
import br.com.ra.complaints.application.web.controller.responses.ComplaintResponse;
import br.com.ra.complaints.domain.service.ComplaintService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

import static br.com.ra.complaints.application.utis.Constants.*;

@Api(value = "Complaints",tags = "Complaints")
@Validated
@RestController
@RequestMapping(value = "/api/complaints", consumes = MediaType.APPLICATION_JSON_VALUE ,produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class ComplaintController {

    private final ComplaintService service;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Creation of the Complaint", tags = "Complaints")
    public void create(@RequestBody @Valid ComplaintRequest request){
        service.create(request.toComplaint());
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Updating of the Complaint", tags = "Complaints")
    public void update(@RequestBody @Valid ComplaintRequest request, @PathVariable String id){
        service.update(request.toComplaint(),id);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Retrieving of the Complaint by id", tags = "Complaints")
    public ComplaintResponse getComplain(@PathVariable String id){
        return ComplaintResponse.toResponse(service.getComplaint(id));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(params = {"page","size"})
    @ApiOperation(value = "Retrieving all Complaints with Pagination", tags = "Complaints")
    public List<ComplaintResponse> getComplains(@RequestParam @NotNull(message = PAGE_CAN_NOT_BE_NULL) Integer page,
                                                @RequestParam @NotNull(message = SIZE_CAN_NOT_BE_NULL) Integer size){
        return ComplaintResponse.toListResponse(service.getComplaints(page,size));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = "Exclusion of the Complaint by id", tags = "Complaints")
    public void deleteById(@PathVariable String id){
        service.delete(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Count Complaints by Company and Locale", tags = "Company")
    @GetMapping(value = "/count", params = {"company","locale"})
    public ComplaintCountResponse countComplaints(@RequestParam @NotBlank(message = COMPANY_CAN_NOT_BE_BLANK) String company,
                                                  @RequestParam @NotBlank(message = LOCALE_CAN_NOT_BE_BLANK) String locale){
        return ComplaintCountResponse.toResponse(service.countComplaints(company,locale));
    }

    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Count Complaints by Locale", tags = "Locale")
    @GetMapping(value = "/count", params = "locale")
    public ComplaintCountResponse countComplaints(@RequestParam @NotBlank(message = LOCALE_CAN_NOT_BE_BLANK) String locale){
        return ComplaintCountResponse.toResponse(service.countComplaints(locale));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(params = {"company","locale","page","size"})
    @ApiOperation(value = "Retrieving all Complaints by Company and Locale with Pagination", tags = "Company")
    public List<ComplaintResponse> getComplains(@RequestParam @NotBlank(message = COMPANY_CAN_NOT_BE_BLANK) String company,
                                                @RequestParam @NotBlank(message = LOCALE_CAN_NOT_BE_BLANK) String locale,
                                                @RequestParam @NotNull(message = PAGE_CAN_NOT_BE_NULL) Integer page,
                                                @RequestParam @NotNull(message = SIZE_CAN_NOT_BE_NULL) Integer size) {
        return ComplaintResponse.toListResponse(service.getComplaints(company,locale,page,size));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(params = {"company","page","size"})
    @ApiOperation(value = "Retrieving all Complaints by Company with Pagination", tags = "Company")
    public List<ComplaintResponse> getComplainsByCompany(@RequestParam @NotBlank(message = COMPANY_CAN_NOT_BE_BLANK) String company,
                                                @RequestParam @NotNull(message = PAGE_CAN_NOT_BE_NULL) Integer page,
                                                @RequestParam @NotNull(message = SIZE_CAN_NOT_BE_NULL) Integer size) {
        return ComplaintResponse.toListResponse(service.getComplaintsByCompany(company, page, size));
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(params = {"locale","page","size"})
    @ApiOperation(value = "Retrieving all Complaints by Locale with Pagination", tags = "Locale")
    public List<ComplaintResponse> getComplainsByLocale(@RequestParam @NotBlank(message = LOCALE_CAN_NOT_BE_BLANK) String locale,
                                                @RequestParam @NotNull(message = PAGE_CAN_NOT_BE_NULL) Integer page,
                                                @RequestParam @NotNull(message = SIZE_CAN_NOT_BE_NULL) Integer size) {
        return ComplaintResponse.toListResponse(service.getComplaintsByLocale(locale, page, size));
    }
}
