package br.com.ra.complaints.application.web.controller.responses;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ComplaintCountResponse {
    private Integer total;

    public static ComplaintCountResponse toResponse(Integer total){
        return ComplaintCountResponse.builder().total(total).build();
    }
}
