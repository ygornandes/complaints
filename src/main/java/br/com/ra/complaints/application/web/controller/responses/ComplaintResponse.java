package br.com.ra.complaints.application.web.controller.responses;

import br.com.ra.complaints.domain.model.Complaint;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Builder
@Getter
@JsonInclude(NON_NULL)
public class ComplaintResponse {
    private String id;
    private String title;
    private String description;
    private String locale;
    private String company;
    private String createdDate;
    private String updatedDate;

    public static List<ComplaintResponse> toListResponse(List<Complaint> complaints){
        return complaints.stream().map(ComplaintResponse::toResponse).collect(Collectors.toList());
    }

    public static ComplaintResponse toResponse(Complaint complaint){
        DateTimeFormatter brazilianDateTime = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss");
        return ComplaintResponse.builder()
                .id(complaint.getId())
                .title(complaint.getTitle())
                .description(complaint.getDescription())
                .locale(complaint.getLocale())
                .company(complaint.getCompany())
                .createdDate(complaint.getCreatedDate().format(brazilianDateTime))
                .updatedDate(Objects.nonNull(complaint.getUpdatedDate()) ? complaint.getUpdatedDate().format(brazilianDateTime) : null)
                .build();
    }
}
