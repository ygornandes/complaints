package br.com.ra.complaints.application.web.controller.responses;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.util.Date;
import java.util.List;

@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErroResponse {
    private Date timestamp;
    private Integer status;
    private String message;
    private List<String> errors;
}
