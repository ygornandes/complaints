package br.com.ra.complaints.application.utis;

public class Constants {
    public static final String PAGE_CAN_NOT_BE_NULL = "Page can not be null.";
    public static final String SIZE_CAN_NOT_BE_NULL = "Size can not be null.";
    public static final String TITLE_CAN_NOT_BE_BLANK = "Title can not be blank.";
    public static final String DESCRIPTION_CAN_NOT_BE_BLANK = "Description can not be blank.";
    public static final String LOCALE_CAN_NOT_BE_BLANK = "Locale can not be blank.";
    public static final String COMPANY_CAN_NOT_BE_BLANK = "Company can not be blank.";
}
