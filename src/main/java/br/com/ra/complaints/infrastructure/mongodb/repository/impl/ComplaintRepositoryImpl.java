package br.com.ra.complaints.infrastructure.mongodb.repository.impl;

import br.com.ra.complaints.domain.model.Complaint;
import br.com.ra.complaints.domain.repository.ComplaintRepository;
import br.com.ra.complaints.infrastructure.mongodb.documents.ComplaintDocument;
import br.com.ra.complaints.infrastructure.mongodb.repository.MongoComplaintRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class ComplaintRepositoryImpl implements ComplaintRepository {

    private final MongoComplaintRepository repository;

    @Override
    public void create(Complaint complaint) {
        repository.save(ComplaintDocument.toDocument(complaint));
    }

    @Override
    public Optional<Complaint> getComplaint(String id) {

        Optional<ComplaintDocument> complaintDocument = repository.findById(id);

        return Optional.ofNullable(complaintDocument.isPresent() ? complaintDocument.get().toComplaint() : null);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<List<Complaint>> getComplaints(Integer page, Integer size) {
        Page<ComplaintDocument> pageComplaints = repository.findAll(PageRequest.of(page, size));

        return Optional.ofNullable(pageComplaints.isEmpty() ? null : toListComplaints(pageComplaints));

    }

    @Override
    public Integer countComplaints(String company, String locale) {
        return repository.countByCompanyAndLocale(company, locale);
    }

    @Override
    public Integer countComplaints(String locale) {
        return repository.countByLocale(locale);
    }

    @Override
    public Optional<List<Complaint>> getComplaints(String company, String locale, Integer page, Integer size) {
        Page<ComplaintDocument> pageComplaints = repository.findByCompanyAndLocale(company,locale,PageRequest.of(page, size));

        return Optional.ofNullable(pageComplaints.isEmpty() ? null : ComplaintDocument.toListComplaint(pageComplaints));
    }

    @Override
    public Optional<List<Complaint>> getComplaintsByCompany(String company, Integer page, Integer size) {
        Page<ComplaintDocument> pageComplaints = repository.findByCompany(company,PageRequest.of(page, size));

        return Optional.ofNullable(pageComplaints.isEmpty() ? null : ComplaintDocument.toListComplaint(pageComplaints));
    }

    @Override
    public Optional<List<Complaint>> getComplaintsByLocale(String locale, Integer page, Integer size) {
        Page<ComplaintDocument> pageComplaints = repository.findByLocale(locale,PageRequest.of(page, size));

        return Optional.ofNullable(pageComplaints.isEmpty() ? null : ComplaintDocument.toListComplaint(pageComplaints));
    }

    @Override
    public void update(Complaint complaint) {
        repository.save(ComplaintDocument.toDocument(complaint));
    }

    private List<Complaint> toListComplaints(Page<ComplaintDocument> pageComplaints) {
        return pageComplaints.get()
                .map(ComplaintDocument::toComplaint)
                .collect(Collectors.toList());
    }


}