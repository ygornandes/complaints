package br.com.ra.complaints.infrastructure.mongodb.repository;

import br.com.ra.complaints.infrastructure.mongodb.documents.ComplaintDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MongoComplaintRepository extends MongoRepository<ComplaintDocument,String> {
    Integer countByCompanyAndLocale(String company, String locale);
    Page<ComplaintDocument> findByCompanyAndLocale(String company, String locale, Pageable pageable);
    Page<ComplaintDocument> findByCompany(String company, Pageable pageable);
    Page<ComplaintDocument> findByLocale(String locale, Pageable pageable);
    Integer countByLocale(String locale);
}
