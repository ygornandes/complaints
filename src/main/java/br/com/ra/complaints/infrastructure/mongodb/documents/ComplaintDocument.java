package br.com.ra.complaints.infrastructure.mongodb.documents;

import br.com.ra.complaints.domain.model.Complaint;
import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Builder
@Document("complaint")
public class ComplaintDocument {
    @Id
    private String id;
    private String title;
    private String description;
    private String locale;
    private String company;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;

    public static ComplaintDocument toDocument(Complaint complaint){
        return ComplaintDocument.builder()
                .id(complaint.getId())
                .title(complaint.getTitle())
                .description(complaint.getDescription())
                .locale(complaint.getLocale())
                .company(complaint.getCompany())
                .createdDate(complaint.getCreatedDate())
                .updatedDate(complaint.getUpdatedDate())
                .build();
    }

    public Complaint toComplaint(){
        return Complaint
                .builder()
                .id(this.getId())
                .title(this.getTitle())
                .description(this.getDescription())
                .locale(this.getLocale())
                .company(this.getCompany())
                .createdDate(this.getCreatedDate())
                .updatedDate(this.getUpdatedDate())
                .build();
    }

    public static List<Complaint> toListComplaint(Page<ComplaintDocument> pageComplaints) {
        return pageComplaints.get()
                .map(ComplaintDocument::toComplaint)
                .collect(Collectors.toList());
    }
}
