package br.com.ra.complaints.domain.service;

import br.com.ra.complaints.domain.model.Complaint;

import java.util.List;

public interface ComplaintService {
    Complaint getComplaint(String id);
    void delete(String id);
    List<Complaint> getComplaints(Integer page, Integer size);
    void create(Complaint complaint);
    void update(Complaint complaint, String id);
    Integer countComplaints(String company, String locale);
    Integer countComplaints(String locale);
    List<Complaint> getComplaints(String company, String locale, Integer page, Integer size);
    List<Complaint> getComplaintsByCompany(String company, Integer page, Integer size);
    List<Complaint> getComplaintsByLocale(String locale, Integer page, Integer size);
}
