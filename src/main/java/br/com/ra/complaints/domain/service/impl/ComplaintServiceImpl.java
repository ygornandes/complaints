package br.com.ra.complaints.domain.service.impl;

import br.com.ra.complaints.application.config.properties.MessagesProperty;
import br.com.ra.complaints.application.exception.ComplaintNotFoundException;
import br.com.ra.complaints.domain.model.Complaint;
import br.com.ra.complaints.domain.repository.ComplaintRepository;
import br.com.ra.complaints.domain.service.ComplaintService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ComplaintServiceImpl implements ComplaintService {

    private final ComplaintRepository repository;
    private final MessagesProperty messages;

    @Override
    public Complaint getComplaint(String id) {
        return repository.getComplaint(id).orElseThrow(() -> new ComplaintNotFoundException(messages.getComplaintNotFound()));
    }

    @Override
    public void delete(String id) {
        repository.delete(id);
    }

    @Override
    public List<Complaint> getComplaints(Integer page, Integer size) {
        return repository.getComplaints(page, size).orElseThrow(() -> new ComplaintNotFoundException(messages.getPageNotFound()));
    }

    @Override
    public void create(Complaint complaint) {
        repository.create(complaint.create());
    }

    @Override
    public void update(Complaint complaint, String id) {
        repository.update(this.getComplaint(id).update(complaint));
    }

    @Override
    public Integer countComplaints(String company, String locale) {
        return repository.countComplaints(company,locale);
    }

    @Override
    public Integer countComplaints(String locale) {
        return repository.countComplaints(locale);
    }

    @Override
    public List<Complaint> getComplaints(String company, String locale, Integer page, Integer size) {
        return repository.getComplaints(company,locale,page,size).orElseThrow(() -> new ComplaintNotFoundException(messages.getPageNotFound()));
    }

    @Override
    public List<Complaint> getComplaintsByCompany(String company, Integer page, Integer size) {
        return repository.getComplaintsByCompany(company,page,size).orElseThrow(() -> new ComplaintNotFoundException(messages.getPageNotFound()));
    }

    @Override
    public List<Complaint> getComplaintsByLocale(String locale, Integer page, Integer size) {
        return repository.getComplaintsByLocale(locale,page,size).orElseThrow(() -> new ComplaintNotFoundException(messages.getPageNotFound()));
    }
}
