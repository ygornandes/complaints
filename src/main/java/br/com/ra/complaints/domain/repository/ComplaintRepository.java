package br.com.ra.complaints.domain.repository;

import br.com.ra.complaints.domain.model.Complaint;

import java.util.List;
import java.util.Optional;

public interface ComplaintRepository {
    void create(Complaint complaint);
    Optional<Complaint> getComplaint(String id);
    void delete(String id);
    Optional<List<Complaint>> getComplaints(Integer page, Integer size);
    void update(Complaint complaint);
    Integer countComplaints(String company, String locale);
    Integer countComplaints(String locale);
    Optional<List<Complaint>> getComplaints(String company, String locale, Integer page, Integer size);
    Optional<List<Complaint>> getComplaintsByCompany(String company, Integer page, Integer size);
    Optional<List<Complaint>> getComplaintsByLocale(String locale, Integer page, Integer size);
}
