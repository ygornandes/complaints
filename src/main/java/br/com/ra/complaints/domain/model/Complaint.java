package br.com.ra.complaints.domain.model;

import lombok.Builder;
import lombok.Getter;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Getter
public class Complaint {
    private String id;
    private String title;
    private String description;
    private String locale;
    private String company;
    private LocalDateTime createdDate;
    private LocalDateTime updatedDate;

    public Complaint update(Complaint complaint){
        this.title = merge(complaint.getTitle(),this.getTitle());
        this.description = merge(complaint.getDescription(),this.getDescription());
        this.locale = merge(complaint.getLocale(), this.getLocale());
        this.company = merge(complaint.getCompany(),this.getCompany());
        this.updatedDate = LocalDateTime.now();
        return this;
    }

    private String merge(String old, String update) {
        return StringUtils.isEmpty(update) ? old : update;
    }

    public Complaint create(){
        this.createdDate = LocalDateTime.now();
        this.id = UUID.randomUUID().toString();
        return this;
    }
}
