# Complaints API

Api de Gerenciamento de Reclamações

# Descricão

O projeto de Gerenciamento de Reclamações é uma Api Rest que tem o intuito de atender aos requisitos propostos pela Teste. 
Foi Utilizado o framework Spring Boot para criação dos microserviços e DDD para arquitetura do projeto, e decidi utiliza-los para agilizar o desenvolvimento da API, para que o foco principal fique nas regras de negócios e nos testes unitarios.

# Instruções para utilização:

Para buildar a aplicação usar o comando abaixo:

- ./gradlew clean build docker

Para executar a API e subir o MongoDB localmente foi utilizado o Docker compose, utilize a linha de comando para executar os seguintes comandos:

- docker-compose up -d 

Para desligar a aplicação usar o comando abaixo:

- docker-compose down

# Deploy na Cloud

- Utilizar um CI com compatibilidade de utilização de container docker. 

# Documentação Swagger

- http://localhost:5000/swagger-ui.html
 
# Arquitetura

- Micro Serviços
- DDD
  
# Tecnologias utilizadas

- Java 8
- Spring Boot
- Spring Data
- Spring Actuator
- Spring Devtools
- Tomcat embutido
- MongoDB
- Docker
- Swagger
- Mockito
- JUnit
- Lombok

# Ferramentas utilizadas:

- IntelliJ
- Gradle
- Git
- Java 1.8
- Docker
- Studio 3T
